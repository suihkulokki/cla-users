cla-users
======

A tool to check gitlab submitter and the git pull
request against a whitelist of approved users

Users sign CLA by sending a pull request against
this repository, adding their email to the users.txt file

The actual repository needs a .gitlab-ci.yml in the form of:

```
stages:
  - .pre
  - test

verify-cla:
  stage: .pre
  image: alpine
  script:
    - apk add git python3 wget
    - wget -q https://url-to/verify-cla.py
    - wget -q https://url-to/users.txt
    - python3 verify-cla.py users.txt

test:
  stage: test
  image: alpine
  script:
    - echo great success!
```
